// Variável constante com o link da API
const url = 'https://viacep.com.br/ws/96045180/json/';
//Função para o requerimento para a API
fetch(url)
    .then(response => response.json())
    .then(data => {
        //Variável alocando todos os parametos do exercício
        var endereco = `${data.logradouro}, 34, ${data.localidade}/${data.uf}`;
        // Saída com os valores para o HTML
        document.getElementById("endereco").innerHTML = endereco;


    })
    .catch(err => console.log(err))